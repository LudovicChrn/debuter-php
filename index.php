<html>
<head>
    <link rel="stylesheet" href="index.css">
    <title>Rock, Paper, Scissors!</title>
    <link rel="stylesheet" href="./font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" href="./chifoumi.jpeg" type="image/x-icon">
</head>
<body>

<?php 
        if (isset($_POST['yourpoints']) && isset($_POST['opppoints'])) {
            $yourpoints = $_POST['yourpoints'];
            $opppoints = $_POST['opppoints'];
        } else {
            $yourpoints = 0;
            $opppoints = 0;
        }
        

        if (isset($_POST['RPS'])) {
            $yourchoice = $_POST["RPS"];
            $opp = array("Rock", "Paper", "Scissors");
            $rand_keys = array_rand($opp);
            $oppchoice = $opp[$rand_keys];

            if (($yourchoice === "Rock" && $oppchoice === "Paper") || ($yourchoice === "Paper" && $oppchoice === "Scissors") || ($yourchoice === "Scissors" && $oppchoice === "Rock")) {
                $results = "Loose";
                $opppoints++;
            } else if (($yourchoice === "Rock" && $oppchoice === "Scissors") || ($yourchoice === "Paper" && $oppchoice === "Rock") || ($yourchoice === "Scissors" && $oppchoice === "Paper")) {
                $results = "Win";
                $yourpoints++;
            } else {
                $results = "Tie";
            }
        }
?>
<div class="game__content">
<form action="" method="POST" class="game__menu">
        <input type="hidden" name="yourpoints" value="<?php echo $yourpoints; ?>" />
        <input type="hidden" name="opppoints" value="<?php echo $opppoints; ?>" />
        <label class="choose" for="RPS">Choose a start</label>

        <div>
        <button name="RPS" value="Rock"><i class="fa fa-hand-rock-o"></i></button>
        <button name="RPS" value="Paper"><i class="fa fa-hand-paper-o"></i></button>
        <button name="RPS" value="Scissors"><i class="fa fa-hand-scissors-o"></i></button>  
        </div>
</form>



    <div class="game__result">
        <?php 

        if (isset($_POST['RPS'])) {
            
            if ($results === "Loose"){
                echo "<p class='choice'>You choosed: </p>" . "<i style='color: red' id='font_result' class='fa fa-hand-" . $yourchoice . "-o'></i>";
                echo "<p class='choice'>Your opponent choosed: </p>" . "<i style='color: green' id='font_result' class='fa fa-hand-" . $oppchoice . "-o'></i>";
            } else if ($results === "Win") {
                echo "<p class='choice'>You choosed: </p>" . "<i style='color: green' id='font_result' class='fa fa-hand-" . $yourchoice . "-o'></i>";
                echo "<p class='choice'>Your opponent choosed: </p>" . "<i style='color: red' id='font_result' class='fa fa-hand-" . $oppchoice . "-o'></i>";
            } else {
                echo "<p class='choice'>You choosed: </p>" . "<i style='color: blue' id='font_result' class='fa fa-hand-" . $yourchoice . "-o'></i>";
                echo "<p class='choice'>Your opponent choosed: </p>" . "<i style='color: blue' id='font_result' class='fa fa-hand-" . $oppchoice . "-o'></i>";
            }
        }
        ?>
        </div>

    <div class="game__points">
        <h4>Points</h4>
        <table>
            <tr>
                <th>Yours</th>
                <th>Opponent</th>
            </tr>
            <tr>
                <td><?php echo $yourpoints; ?></td>
                <td><?php echo $opppoints; ?></td>
            </tr>
        </table>
    </div>

    
</div>


</body>
</html>